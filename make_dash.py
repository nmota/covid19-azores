import csv
import pandas as pd

from bokeh.plotting import figure, output_file, show
from bokeh.themes import built_in_themes
from bokeh.layouts import layout, column, row
from bokeh.models import BooleanFilter, CDSView, ColumnDataSource, LabelSet
from bokeh.models import DatetimeTickFormatter, Panel, Tabs
from bokeh.models.tickers import FixedTicker
from bokeh.tile_providers import CARTODBPOSITRON_RETINA, get_provider
from bokeh.embed import components
from math import log
from jinja2 import Environment, FileSystemLoader


# prepare data
data = pd.read_csv('data/dados.csv', sep=';')

data['data'] = pd.to_datetime(data['data'], format='%d/%m/%y %H:%M')

last_date = data['data'].max()

source = ColumnDataSource(data=data)

# Criar gráfico RAA

source_raa = data[data['local'] == 'RAA']

s1 = figure(title="Evolução temporal",
            x_axis_label='dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# Add plot line with circles

s1.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_raa)
s1.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_raa)
s1.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_raa)
s1.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_raa)
s1.hover.point_policy = "snap_to_data"
s1.hover.formatters = {"@data": "datetime"}
s1.height = 290
tab1 = Panel(child=s1, title="RAA")

# Criar gráfico S.Miguel

source_smg = data[data['local'] == 'São Miguel']

s3 = figure(title="Evolução temporal",
            x_axis_label='dias dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# add a line renderer with legend and line thickness
s3.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_smg)
s3.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_smg)
s3.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_smg)
s3.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_smg)
s3.hover.formatters = {"@data": "datetime"}
s3.hover.point_policy = "snap_to_data"

s3.height = 290
tab2 = Panel(child=s3, title="São Miguel")

# Criar gráfico Terceira

source_ter = data[data['local'] == 'Terceira']

s4 = figure(title="Evolução temporal",
            x_axis_label='dias dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# add a line renderer with legend and line thickness
s4.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_ter)
s4.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_ter)
s4.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_ter)
s4.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_ter)
s4.hover.formatters = {"@data": "datetime"}
s4.hover.point_policy = "snap_to_data"

s4.height = 290
tab3 = Panel(child=s4, title="Terceira")

# Criar gráfico Santa Maria

source_sma = data[data['local'] == 'Santa Maria']

s5 = figure(title="Evolução temporal",
            x_axis_label='dias dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# add a line renderer with legend and line thickness
s5.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_sma)
s5.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_sma)
s5.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_sma)
s5.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_sma)
s5.hover.formatters = {"@data": "datetime"}
s5.hover.point_policy = "snap_to_data"

s5.height = 290
tab4 = Panel(child=s5, title="Santa Maria")

# Criar gráfico Graciosa

source_grw = data[data['local'] == 'Graciosa']

s6 = figure(title="Evolução temporal",
            x_axis_label='dias dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# add a line renderer with legend and line thickness
s6.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_grw)
s6.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_grw)
s6.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_grw)
s6.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_grw)
s6.hover.formatters = {"@data": "datetime"}
s6.hover.point_policy = "snap_to_data"

s6.height = 290
tab5 = Panel(child=s6, title="Graciosa")

# Criar gráfico São Jorge

source_sjz = data[data['local'] == 'São Jorge']

s7 = figure(title="Evolução temporal",
            x_axis_label='dias dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# add a line renderer with legend and line thickness
s7.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_sjz)
s7.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_sjz)
s7.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_sjz)
s7.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_sjz)
s7.hover.formatters = {"@data": "datetime"}
s7.hover.point_policy = "snap_to_data"

s7.height = 290
tab6 = Panel(child=s7, title="São Jorge")

# Criar gráfico Pico

source_pix = data[data['local'] == 'Pico']

s8 = figure(title="Evolução temporal",
            x_axis_label='dias dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# add a line renderer with legend and line thickness
s8.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_pix)
s8.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_pix)
s8.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_pix)
s8.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_pix)
s8.hover.formatters = {"@data": "datetime"}
s8.hover.point_policy = "snap_to_data"

s8.height = 290
tab7 = Panel(child=s8, title="Pico")

# Criar gráfico Faial

source_hor = data[data['local'] == 'Faial']

s9 = figure(title="Evolução temporal",
            x_axis_label='dias dias a partir de 15-3-2020',
            y_axis_label='nº',
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='lineplot',
            tooltips=[
                ("valor", "$y{0}"),
                ("data", '@data{%F}')
            ])

# add a line renderer with legend and line thickness
s9.line('id', 'positivos', legend_label="Positivos",
        line_width=2, line_color='#2196F3', source=source_hor)
s9.circle('id', 'positivos', legend_label="Positivos",
          fill_color="white", line_color='#2196F3', size=8, source=source_hor)
s9.line('id', 'suspeitos', legend_label="Suspeitos",
        line_width=2, line_color='#ff9800', source=source_hor)
s9.circle('id', 'suspeitos', legend_label="Suspeitos",
          fill_color="white", line_color='#ff9800', size=8, source=source_hor)
s9.hover.formatters = {"@data": "datetime"}
s9.hover.point_policy = "snap_to_data"

s9.height = 290
tab8 = Panel(child=s9, title="Faial")

# Criar gráfico Flores

source_flw = data[data['local'] == 'Flores']

s10 = figure(title="Evolução temporal",
             x_axis_label='dias dias a partir de 15-3-2020',
             y_axis_label='nº',
             toolbar_location=None,
             tools='hover',
             sizing_mode="stretch_both",
             name='lineplot',
             tooltips=[
                 ("valor", "$y{0}"),
                 ("data", '@data{%F}')
             ])

# add a line renderer with legend and line thickness
s10.line('id', 'positivos', legend_label="Positivos",
         line_width=2, line_color='#2196F3', source=source_flw)
s10.circle('id', 'positivos', legend_label="Positivos",
           fill_color="white", line_color='#2196F3', size=8, source=source_flw)
s10.line('id', 'suspeitos', legend_label="Suspeitos",
         line_width=2, line_color='#ff9800', source=source_flw)
s10.circle('id', 'suspeitos', legend_label="Suspeitos",
           fill_color="white", line_color='#ff9800', size=8, source=source_flw)
s10.hover.formatters = {"@data": "datetime"}
s10.hover.point_policy = "snap_to_data"

s10.height = 290
tab9 = Panel(child=s10, title="Flores")

# Criar gráfico Corvo

source_cvu = data[data['local'] == 'Corvo']

s11 = figure(title="Evolução temporal",
             x_axis_label='dias dias a partir de 15-3-2020',
             y_axis_label='nº',
             toolbar_location=None,
             tools='hover',
             sizing_mode="stretch_both",
             name='lineplot',
             tooltips=[
                 ("valor", "$y{0}"),
                 ("data", '@data{%F}')
             ])

# add a line renderer with legend and line thickness
s11.line('id', 'positivos', legend_label="Positivos",
         line_width=2, line_color='#2196F3', source=source_cvu)
s11.circle('id', 'positivos', legend_label="Positivos",
           fill_color="white", line_color='#2196F3', size=8, source=source_cvu)
s11.line('id', 'suspeitos', legend_label="Suspeitos",
         line_width=2, line_color='#ff9800', source=source_cvu)
s11.circle('id', 'suspeitos', legend_label="Suspeitos",
           fill_color="white", line_color='#ff9800', size=8, source=source_cvu)
s11.hover.formatters = {"@data": "datetime"}
s11.hover.point_policy = "snap_to_data"

s11.height = 290
tab10 = Panel(child=s11, title="Corvo")

# tabs

tabs = Tabs(tabs=[tab1, tab2, tab3, tab4, tab5, tab6, tab7, tab8, tab9, tab10])

# MAP PLOT

# MAP DATA
tile_provider = get_provider(CARTODBPOSITRON_RETINA)

map_data1 = data[data['local'] != 'RAA']
map_data = map_data1[map_data1['data'] == map_data1['data'].max()]

source_map = ColumnDataSource(data=map_data)

# range bounds supplied in web mercator coordinates
# Map1 - Positivos
m2 = figure(x_range=(-3500000, -2800000),
            y_range=(4400000, 4850000),
            x_axis_type="mercator",
            y_axis_type="mercator",
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='map',
            tooltips=[('ilha', '@local'), ('positivos', '@positivos')])

m2.add_tile(tile_provider)

m2.circle(x='lon', y='lat', size=30,
          fill_color='#2196F3', fill_alpha=0.5, line_color='#2196F3', source=source_map)

labels = LabelSet(x='lon', y='lat', text='positivos',
                  text_baseline="middle", text_align="center", text_color="white", source=source_map)

m2.xaxis.visible = False
m2.yaxis.visible = False
m2.height = 290
m2.add_layout(labels)

map_tab2 = Panel(child=m2, title="Positivos")

# Map1 - Suspeitos

m1 = figure(x_range=(-3500000, -2800000),
            y_range=(4400000, 4850000),
            x_axis_type="mercator",
            y_axis_type="mercator",
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='map',
            tooltips=[('ilha', '@local'), ('suspeitos', '@suspeitos')])

m1.add_tile(get_provider(CARTODBPOSITRON_RETINA))

m1.circle(x='lon', y='lat', size=30,
          fill_color='#ff9800', fill_alpha=0.5, line_color='#ff9800', source=source_map)

labels = LabelSet(x='lon', y='lat', text='suspeitos',
                  text_baseline="middle", text_align="center", text_color="white", source=source_map)

m1.xaxis.visible = False
m1.yaxis.visible = False
m1.height = 290
m1.add_layout(labels)

map_tab1 = Panel(child=m1, title="Suspeitos")

# Map3 - Vigilância
m3 = figure(x_range=(-3500000, -2800000),
            y_range=(4400000, 4850000),
            x_axis_type="mercator",
            y_axis_type="mercator",
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='map',
            tooltips=[('ilha', '@local'), ('vigilância', '@vigilância')])

m3.add_tile(get_provider(CARTODBPOSITRON_RETINA))

m3.circle(x='lon', y='lat', size=30,
          fill_color='#ff5722', fill_alpha=0.5,  line_color='#ff5722', source=source_map)

labels = LabelSet(x='lon', y='lat', text='vigilancia',
                  text_baseline="middle", text_align="center", text_color="white", source=source_map)

m3.xaxis.visible = False
m3.yaxis.visible = False
m3.height = 290
m3.add_layout(labels)

map_tab3 = Panel(child=m3, title="Vigilância")

# Map4 - Óbitos
m4 = figure(x_range=(-3500000, -2800000),
            y_range=(4400000, 4850000),
            x_axis_type="mercator",
            y_axis_type="mercator",
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='map',
            tooltips=[('ilha', '@local'), ('óbitos', '@obitos')])

m4.add_tile(get_provider(CARTODBPOSITRON_RETINA))

m4.circle(x='lon', y='lat', size=30,
          fill_color='#607d8b', fill_alpha=0.5, line_color='#607d8b', source=source_map)

labels = LabelSet(x='lon', y='lat', text='obitos',
                  text_baseline="middle", text_align="center", text_color="white", source=source_map)

m4.xaxis.visible = False
m4.yaxis.visible = False
m4.height = 290
m4.add_layout(labels)

map_tab4 = Panel(child=m4, title="Óbitos")

# Map5 - Recuperados
m4 = figure(x_range=(-3500000, -2800000),
            y_range=(4400000, 4850000),
            x_axis_type="mercator",
            y_axis_type="mercator",
            toolbar_location=None,
            tools='hover',
            sizing_mode="stretch_both",
            name='map',
            tooltips=[('ilha', '@local'), ('recuperados', '@recuperados')])

m4.add_tile(get_provider(CARTODBPOSITRON_RETINA))

m4.circle(x='lon', y='lat', size=30,
          fill_color='#4CAF50', fill_alpha=0.5, line_color='#4CAF50', source=source_map)

labels = LabelSet(x='lon', y='lat', text='recuperados',
                  text_baseline="middle", text_align="center", text_color="white", source=source_map)

m4.xaxis.visible = False
m4.yaxis.visible = False
m4.height = 290
m4.add_layout(labels)

map_tab5 = Panel(child=m4, title="Recuperados")

map_tabs = Tabs(tabs=[map_tab2, map_tab1, map_tab3, map_tab4, map_tab5])

# LAST RECORD

last_record = data[(data['local'] == 'RAA') & (
    data['data'] == data['data'].max())]

# output to static HTML file

file_loader = FileSystemLoader('templates')
env = Environment(loader=file_loader)

map_div, map_script = components(map_tabs)
plot_div, plot_script = components(tabs)

static_template = env.get_template('dash.html')

output = static_template.render(plot=plot_div,
                                map=map_div,
                                script_plot=plot_script,
                                script_map=map_script,
                                casos=last_record['positivos'].values[0],
                                suspeitos=last_record['suspeitos'].values[0],
                                vigilancia=last_record['vigilancia'].values[0],
                                obitos=last_record['obitos'].values[0],
                                recuperados=last_record['recuperados'].values[0],
                                last_date=last_date)

with open('public/index.html', 'wb') as file:
    file.write(output.encode('utf-8'))
